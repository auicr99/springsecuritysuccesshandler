package com.successhandler.service;

import com.successhandler.data.Customer;
import com.successhandler.jpa.entities.CustomerEntity;

public interface CustomerService {
    CustomerEntity saveCustomer(final Customer customer);
}
