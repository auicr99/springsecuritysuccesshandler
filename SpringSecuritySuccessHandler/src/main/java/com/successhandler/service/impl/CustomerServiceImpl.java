package com.successhandler.service.impl;

import com.successhandler.data.Customer;
import com.successhandler.jpa.entities.CustomerEntity;
import com.successhandler.jpa.repository.CustomerRepository;
import com.successhandler.service.CustomerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service("customerService")
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public CustomerEntity saveCustomer(final Customer customer){
        CustomerEntity customerModel = populateCustomerData(customer);
        return customerRepository.save(customerModel);
    }

    private CustomerEntity populateCustomerData(final Customer customer){
        CustomerEntity customerEnitity = new CustomerEntity();
        customer.setFirstName(customer.getFirstName());
        customer.setLastName(customer.getLastName());
        customer.setEmail(customer.getEmail());
        customer.setPassword(passwordEncoder.encode(customer.getPassword()));
        return customerEnitity;
    }
}
